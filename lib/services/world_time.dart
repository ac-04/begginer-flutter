import 'package:http/http.dart';
import 'dart:convert';
import 'package:intl/intl.dart';
class World_time {

  String location;
  String time;
  String flag; // url to flag icon
  String url;
  bool isDayTime;
  World_time({this.location,this.flag,this.url}){
    this.flag = 'https://www.countryflags.io/${this.flag}/flat/64.png';
  }

  Future<void> getTime() async {
  try{
    //make request
    Response response = await get('http://worldtimeapi.org/api/timezone/${url}');
    Map data = jsonDecode(response.body);
    //set the flag
    //get properties form data
    String datetime = data["datetime"];
    String offset = data["utc_offset"].substring(1,3);

    //print('${datetime} ${offset}');
    // convert to datetime object
    DateTime now = DateTime.parse(datetime);
    now.add(Duration(hours: int.parse(offset)));
    //set the time
    this.isDayTime = (now.hour > 6 && now.hour < 15);
    this.time = DateFormat.jm().format(now);
  }catch(e){
    this.time = "couldn't get time from the timezone provided";
  }
  }

}