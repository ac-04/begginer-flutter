import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:worldtimeapp/services/world_time.dart';

class ChooseLocation extends StatefulWidget {
  @override
  _ChooseLocationState createState() => _ChooseLocationState();
}

class _ChooseLocationState extends State<ChooseLocation> {

  List<World_time> locations = [
    World_time(url: 'America/Los_Angeles',flag: 'us',location: 'Los Angeles'),
    World_time(url: 'America/El_Salvador',flag: 'sv',location: 'San Salvador')
  ];

  int counter = 0;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    debugPrint("Init State runs firts");
  }

  void updateTime(index) async {
  Navigator.pushReplacementNamed(context, "/",arguments: locations[index]);
  }

  @override
  Widget build(BuildContext context) {
    debugPrint("build runs later");
    return Scaffold(
      backgroundColor: Colors.grey,
      appBar: AppBar(
        backgroundColor: Colors.blue[900],
        title: Text("Choose location"),
        centerTitle: true,
        elevation: 0,
      ),
      body: ListView.builder(
        itemCount: locations.length,
        itemBuilder: (context,index){
          return Card(
            child: ListTile(
              onTap: (){
                this.updateTime(index);
              },
              title: Text(locations[index].location),
              leading: CircleAvatar(
                backgroundImage: NetworkImage(locations[index].flag),
              ),
            ),
          );
        },
      ),
    );
  }
}
