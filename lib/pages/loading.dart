import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'package:worldtimeapp/services/world_time.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Loading extends StatefulWidget {
  @override
  _LoadingState createState() => _LoadingState();
}

class _LoadingState extends State<Loading> {

  void setupWorldTime(wt)async {
    World_time instance = wt;
    await instance.getTime();
    Navigator.pushReplacementNamed(context, '/home',arguments: {
      'location':instance.location,
      'flag' : instance.flag,
      'time':instance.time,
      'isDayTime':instance.isDayTime
    });
  }
  dynamic data = {};

  @override
  Widget build(BuildContext context) {

    data = ModalRoute.of(context).settings.arguments;
    if(this.data!=null){
      print(this.data);
      this.setupWorldTime(this.data);
    }else{
      this.setupWorldTime(World_time(location: 'Berlin',flag: 'de',url: 'Europe/berlin'));
    }

    return Scaffold(
      backgroundColor: Colors.blue[900],
        body: Center(
          child: SpinKitFadingCube(
            color: Colors.white,
            size: 50.0,
          ),
        )
    );
  }
}
