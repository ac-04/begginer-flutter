import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  Map data = {};

  @override
  Widget build(BuildContext context) {
    data = ModalRoute.of(context).settings.arguments;
    String bgImage = data['isDayTime'] ? 'https://papers.co/wallpaper/papers.co-ml99-hot-sunny-day-nature-farm-40-wallpaper.jpg' : 'https://papers.co/wallpaper/papers.co-oc33-moon-sky-night-nature-40-wallpaper.jpg';
    return Scaffold(
      body: SafeArea(
          child:Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: NetworkImage(bgImage),
                fit:BoxFit.cover
              )
            ),
            child: Padding(
        padding: const EdgeInsets.fromLTRB(0,120.0,0,0),
        child: Column(
            children: [
              FlatButton.icon(
                  onPressed: (){
                    Navigator.pushNamed(context, '/choose');
                  },
                  icon: Icon(Icons.edit_location,color: Colors.white,),
                  label: Text("Edit location",style: TextStyle(
                    color: Colors.white
                  ),)
              ),
              SizedBox(height: 20.0,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(data['location'],style: TextStyle(
                    fontSize: 28,
                    letterSpacing: 2.0,
                    color: Colors.white
                  ),),
                ],
              ),
              SizedBox(height: 20.0,),
              Text(data['time'],style:
                TextStyle(
                  fontSize: 66.0,
                  color:Colors.white
                ),)
            ],
        ),
      ),
          )),
    );
  }
}
